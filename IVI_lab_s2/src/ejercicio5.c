#include <GL/glut.h>

static GLfloat rotate = 0;

/*Grados de giro en eje x, y , z*/
static GLfloat deg_axis_x=0;
static GLfloat deg_axis_y=0; 
static GLfloat deg_axis_z=0;


void render () { 
  /* Limpieza de buffers */
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  /* Carga de la matriz identidad */
  glLoadIdentity();
  /* Posición de la cámara virtual (position, look, up) */
  gluLookAt(0.0, 0.0, 5.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
  
  /* En color blanco */
  glColor3f( 1.0, 1.0, 1.0 );  

  /*Rotacion de la tetera*/
  glRotatef(deg_axis_x, 1.0, 0.0, 0.0);
  glRotatef(deg_axis_y, 0.0, 1.0, 0.0);
  glRotatef(deg_axis_z, 0.0, 0.0, 1.0);

  /* Renderiza la tetera */
  glutWireTeapot(1.5);
  /* Intercambio de buffers... Representation ---> Window */
  glutSwapBuffers();      
} 

void resize (int w, int h) { 
  /* Definición del viewport */
  glViewport(0, 0, w, h);

  /* Cambio a transform. vista */
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  /* Actualiza el ratio ancho/alto */
  gluPerspective(50., w/(double)h, 1., 10.);

  /* Vuelta a transform. modelo */
  glMatrixMode(GL_MODELVIEW);
}

void sense (int key,int x,int y){
  switch (key){
    case GLUT_KEY_RIGHT:
      rotate=1.0;
      break;
    case GLUT_KEY_LEFT:
      rotate=-1.0;
      break;
  }
}

void rotate_x (){
  if (rotate==1.0){
    deg_axis_x+=1;
  }
  else {
    deg_axis_x-=1;
  }
  glutPostRedisplay();
}

void rotate_y (){
  if (rotate==1.0){
    deg_axis_y+=1;
  }
  else {
    deg_axis_y-=1;
  }
  glutPostRedisplay();
}

void rotate_z (){
  if (rotate==1.0){
    deg_axis_z+=1;
  }
  else {
    deg_axis_z-=1;
  }
  glutPostRedisplay();
}

void axis_rotation (unsigned char key, int x, int y) { 

  switch (key){
    case 'x':
      
      glutIdleFunc(rotate_x);
      break;
    case 'y':
      
      glutIdleFunc(rotate_y);
      break;
    case 'z':
      
      glutIdleFunc(rotate_z);
      break;
    case 's':
      glutIdleFunc(NULL);
      break;
  }


}

int main (int argc, char* argv[]) { 
  glutInit( &argc, argv ); 
  glutInitDisplayMode( GLUT_RGB | GLUT_DOUBLE ); 
  glutInitWindowSize(640, 480); 
  glutCreateWindow( "IVI - Sesion 2" ); 
  glEnable (GL_DEPTH_TEST);
  
  /* Registro de funciones de retrollamada */
  glutDisplayFunc(render); 
  glutReshapeFunc(resize); 
  

  glutKeyboardFunc(axis_rotation);
  glutSpecialFunc(sense);

  /* Bucle de renderizado */
  glutMainLoop();  
  
  return 0; 
} 
