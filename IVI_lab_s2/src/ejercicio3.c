#include <GL/glut.h>

int state = 1;
/* Función de renderizado */
void render () {
  /* Limpieza de buffers */
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  /* Carga de la matriz identidad */
  glLoadIdentity();
  /* Traslación */
  glTranslatef(0.0, 0.0, -4.0);

  /* Renderiza un triángulo blanco */
  glBegin(GL_TRIANGLES);
  if (state==1){
    glColor3f(1.0,1.0,1.0);
    
    glVertex3f(0.0, 1.0, 0.0);
    glVertex3f(-1.0, -1.0, 0.0);
    glVertex3f(1.0, -1.0, 0.0);
  }
  
  if (state==2){
    glColor3f(0.0,1.0,0.0);
    glVertex3f(0.0, 1.0, 0.0);
    
    glColor3f(0.0,0.0,1.0);
    glVertex3f(-1.0, -1.0, 0.0);
    
    glColor3f(1.0,0.0,0.0);
    glVertex3f(1.0, -1.0, 0.0);

  }
  
  glEnd();

  /* Intercambio de buffers */
  glutSwapBuffers();
}

void menu(int num){
  switch(num){
    case 1:
      state=1;
    break;
    case 2:
      state=2;
    break;
  }
  glutPostRedisplay();

}

void keyc(unsigned char key, int x, int y){
  switch(key){
  case 'c':
    if(state==1){
      state=2;
    }else{
      state=1;
    }
      glutPostRedisplay();
      break;
    }
}

void resize (int w, int h) {
  /* Definición del viewport */
  glViewport(0, 0, w, h);

  /* Cambio a transform. vista */
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  /* Actualiza el ratio ancho/alto */
  gluPerspective(50., w/(double)h, 1., 10.);

  /* Vuelta a transform. modelo */
  glMatrixMode(GL_MODELVIEW);
}

void init (void) {
  glEnable(GL_DEPTH_TEST);
}

int main(int argc, char *argv[]) {
  glutInit(&argc, argv);

  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
  glutInitWindowSize(400, 400);
  glutInitWindowPosition(200, 200);

  glutCreateWindow("Hola Mundo con OpenGL!");

  init();

  /* Funcion para registrar funcionamiento de letra c*/
  glutKeyboardFunc(keyc);

  /*Creación y uso del menú*/
  glutCreateMenu(menu);
  glutAddMenuEntry("White triangle", 1);
  glutAddMenuEntry("Three colour triangle", 2);

  /*Definición de los 3 botones del ratón*/  
  glutAttachMenu(GLUT_LEFT_BUTTON);
  glutAttachMenu(GLUT_MIDDLE_BUTTON);
  glutAttachMenu(GLUT_RIGHT_BUTTON);

  /* Registro de funciones de retrollamada */
  glutDisplayFunc(render);
  glutReshapeFunc(resize);

  /* Bucle de renderizado */
  glutMainLoop();

  return 0;
}