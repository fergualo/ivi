#include <GL/glut.h>    
#include <AR/gsub.h>    
#include <AR/video.h>   
#include <AR/param.h>   
#include <AR/ar.h>
#include <AR/arMulti.h>

/*Grados de giro en eje x, y , z*/
static GLfloat deg_axis_x=90;
static GLfloat deg_axis_y=90; 
static GLfloat deg_axis_z=90;

static GLfloat rotate=90;

/*Tamaño de la tetera (por defecto) e índices*/
static GLfloat size_default=90;
static GLfloat size_big=90; 
static GLfloat size_small=90;

// ==== Definicion de constantes y variables globales ===============
ARMultiMarkerInfoT  *mMarker;       // Estructura global Multimarca

void print_error (char *error) {  printf("%s\n",error); exit(0); }
// ======== cleanup =================================================
static void cleanup(void) {   // Libera recursos al salir ...
  arVideoCapStop();   arVideoClose();   argCleanup();   exit(0);
}

// ======== keyboard ================================================
static void keyboard(unsigned char key, int x, int y) {
  switch (key) {
  case 0x1B: case 'Q': case 'q':  cleanup(); break;
  }
}

// ======== rotation ===============================================

void rotate_x (){
  if (rotate==90){
    deg_axis_x+=1;
  }
  else {
    deg_axis_x-=1;
  }
  glutPostRedisplay();
}

void rotate_y (){
  if (rotate==90){
    deg_axis_y+=1;
  }
  else {
    deg_axis_y-=1;
  }
  glutPostRedisplay();
}

void rotate_z (){
  if (rotate==90){
    deg_axis_z+=1;
  }
  else {
    deg_axis_z-=1;
  }
  glutPostRedisplay();
}

// ======== size ====================================================
void size_bigger (){
  if (size_default==90){
  	if (size_big<150){
    	size_big+=0.2;
  	}else{
  		size_big=90;
  	}
  }
  else {
    size_big-=0.2;
  }
  glutPostRedisplay();
}

void size_smaller (){
  if (size_default==90){
  	if (size_small>0){
  		size_small-=0.2;
  	}else{
  		size_small=90;
  	}
  }
  else {
    size_small-=0.2;
  }
  glutPostRedisplay();
}

// ======== draw ====================================================
static void draw( void ) {
  double  gl_para[16];   // Esta matriz 4x4 es la usada por OpenGL
  GLfloat material[]        = {0.0, 0.0, 0.0, 0.0};
  GLfloat light_position[]  = {100.0,-200.0,200.0,0.0};
  int i,c0,c1, c2, c3,c4,c5,c6,c7;
  
  argDrawMode3D();              // Cambiamos el contexto a 3D
  argDraw3dCamera(0, 0);        // Y la vista de la camara a 3D
  glClear(GL_DEPTH_BUFFER_BIT); // Limpiamos buffer de profundidad
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LEQUAL);

  argConvGlpara(mMarker->trans, gl_para);   
  glMatrixMode(GL_MODELVIEW);           
  glLoadMatrixd(gl_para);               

  glEnable(GL_LIGHTING);  glEnable(GL_LIGHT0);
  glLightfv(GL_LIGHT0, GL_POSITION, light_position);
  
  if (1){
    for(i=0; i < mMarker->marker_num; i++) {              
      
      if(mMarker->marker[i].visible < 0){
        //printf ("%i\n", mMarker->marker[i].patt_id);
        switch(mMarker->marker[i].patt_id){
          
          case 0: //Rota en el eje X
            glutIdleFunc(rotate_x);
            c0=1;
          break;

          case 1: //Cambia color a rojo
            material[0]=1.0;
            c1=1;
          break;

          case 2://Cambia color a verde
            material[1]=1.0;
            c2=1;

          break;

          case 3: //Cambia color a azul
            material[2]=1.0;
            c3=1;
          break;

          case 4: //Rota en el eje Y
            glutIdleFunc(rotate_y);
            c4=1;
          break;

          case 5: //Rota en el eje Z
            glutIdleFunc(rotate_z);
            c5=1;
          break;

          case 6: //Aumenta de tamaño 
            glutIdleFunc(size_bigger);
            c6=1;
          break;

          case 7: //Disminuye su tamaño
            glutIdleFunc(size_smaller);
            c7=1;
          break;

        }
      }

    }

    glMaterialfv(GL_FRONT, GL_AMBIENT, material);
    glTranslatef(150.0, -100.0, 60.0);

    if (c1=1){
    	c1=0;
    }else{
    	material[0]=0;
    }
    if (c2=1){
    	c2=0;
    }else{
    	material[1]=0;
    }
    if (c3=1){
    	c3=0;
    }else{
    	material[2]=0;
    }

    if (c0==1){
      glRotatef(deg_axis_x, 1.0, 0.0, 0.0);
      c0=0;
    }else{
      if (c4==1){
        glRotatef(deg_axis_y,0.0,1.0,0.0);
        c4=0;
      }else{
        if (c5==1){
          glRotatef(deg_axis_z,0.0,0.0,1.0);
          c5=0;
        }else{
          glRotatef(rotate, 1.0, 0.0, 0.0);
        }
      }

    }

    if (c6==1){
      glutSolidTeapot(size_big);
      c6=0;
    }else{
      if (c7==1){
        glutSolidTeapot(size_small);
        c7=0;
      }else{
        glutSolidTeapot(size_default);
      }
    }

  }

  glDisable(GL_DEPTH_TEST);
}


// ======== init ====================================================
static void init( void ) {
  ARParam  wparam, cparam;   // Parametros intrinsecos de la camara
  int xsize, ysize;          // Tamano del video de camara (pixels)
  
  // Abrimos dispositivo de video
  if(arVideoOpen("-dev=/dev/video0") < 0) exit(0);  
  if(arVideoInqSize(&xsize, &ysize) < 0) exit(0);

  // Cargamos los parametros intrinsecos de la camara
  if(arParamLoad("data/camera_para.dat", 1, &wparam) < 0)   
    print_error ("Error en carga de parametros de camara\n");
  
  arParamChangeSize(&wparam, xsize, ysize, &cparam);
  arInitCparam(&cparam);   // Inicializamos la camara con "cparam"

  // Cargamos el fichero de especificacion multimarca
  if( (mMarker = arMultiReadConfigFile("data/marker.dat")) == NULL )
    print_error("Error en fichero marker.dat\n");

  argInit(&cparam, 1.0, 0, 0, 0, 0);   // Abrimos la ventana 
}

// ======== mainLoop ================================================
static void mainLoop(void) {
  ARUint8 *dataPtr;
  ARMarkerInfo *marker_info;
  int marker_num;

  // Capturamos un frame de la camara de video
  if((dataPtr = (ARUint8 *)arVideoGetImage()) == NULL) {
    // Si devuelve NULL es porque no hay un nuevo frame listo
    arUtilSleep(2);  return;  // Dormimos el hilo 2ms y salimos
  }

  argDrawMode2D();
  argDispImage(dataPtr, 0,0);    // Dibujamos lo que ve la camara 

  // Detectamos la marca en el frame capturado (return -1 si error)
  if(arDetectMarker(dataPtr, 100, &marker_info, &marker_num) < 0) {
    cleanup(); exit(0);   // Si devolvio -1, salimos del programa!
  }

  arVideoCapNext();      // Frame pintado y analizado... A por otro!
  
  if(arMultiGetTransMat(marker_info, marker_num, mMarker) > 0) 
    draw();       // Dibujamos los objetos de la escena
  
  argSwapBuffers(); // Cambiamos el buffer con lo que tenga dibujado
}

// ======== Main ====================================================
int main(int argc, char **argv) {
  glutInit(&argc, argv);    // Creamos la ventana OpenGL con Glut
  init();                   // Llamada a nuestra funcion de inicio
  
  arVideoCapStart();        // Creamos un hilo para captura de video
  argMainLoop( NULL, keyboard, mainLoop );   // Asociamos callbacks...
  return (0);
}
